Push-Button Stop Nut ("Educated Nut")
=====================================

Press the button for coarse adjustment, turn for fine adjustment.
Useful for quill stop, carriage stop, and similar applications.

This is a variation on the typical "Educated nut" with only three
parts.

You may choose to make graduated index marks in the nut body for
fine adjustment. These are not modeled, and both the pitch and
method of scribing are up to you. However, it will be an educated
nut only if you add some sort of graduated index marks.

The nut is parameterized to allow different sizes and thread forms.
It is critical to include both the proper major and minor pitch
diameters. Bold numbers in the spreadsheet indicate primary numbers
to modify. Italicized numbers in the spreadsheet indicate computed
values, which should be changed only judiciously. After tapping,
you will need to plunge through the button with an endmill as wide
as the major diameter of your lead screw thread; or if that is
not available, slightly larger; keep this in mind while choosing
alternatives.  The nut needs to be tall enough to support the
outside of the thread against the threaded back at the spring end
of the button.

After adjusting parameters (in the spreadsheet "p"), recalculate
the `Pages` group, then review the individual drawing pages for
dimensions that now overlap the drawing and need to be moved to be
legible, or which read 0, comparing them to the sample PDFs. The
console may report dimensions that can no longer be calculated as
a result of the change; those will usually show up as a 0 dimension
on the affected page. Add any dimensions that you will want to use
for your machining operations.

Please feel free to contribute working parameter sets for
different thread pitches.

The provided spring dimensions are for a Delta RP4993 faucet
seat spring; a widely-available conical compression spring of
a reasonably compatible size. If you choose another spring, you
will need to measure its major diameter and minimum (compressed)
length to drive the parameters. The button diameter must equal or
exceed the major diameter of the spring you choose.

You will first drill and tap, then offset and plunge-mill the major
diameter of the thread, then widen it into a slot away from the
remaining threads, to make the free-running hole when you push the
button, while leaving thread form on the spring end of the button.

The hole locations and depths are the critical dimensions.  The
button diameter must be enough larger than the major thread
diameter to support plunging through the button material.  The
location of the hole and offset are critical, so remember to
mill a flat before starting to drill. The outer diameter is
critical with respect to the depths of boring the holes for the
button and spring.

The button should be a smooth sliding fit in the nut.
